// clang-format off
const KnownTrampoline kKnownTrampolines[] = {
{"AAudioStreamBuilder_delete", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_openStream", GetTrampolineFunc<auto(void*, void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setAllowedCapturePolicy", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setAttributionTag", GetTrampolineFunc<auto(void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setBufferCapacityInFrames", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setChannelCount", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setChannelMask", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setContentType", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setDataCallback", GetTrampolineFunc<auto(void*, auto(*)(void*, void*, void*, int32_t) -> int32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setDeviceId", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setDirection", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setErrorCallback", GetTrampolineFunc<auto(void*, auto(*)(void*, void*, int32_t) -> void, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setFormat", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setFramesPerDataCallback", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setInputPreset", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setIsContentSpatialized", GetTrampolineFunc<auto(void*, uint8_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setPackageName", GetTrampolineFunc<auto(void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setPerformanceMode", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setPrivacySensitive", GetTrampolineFunc<auto(void*, uint8_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setSampleRate", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setSamplesPerFrame", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setSessionId", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setSharingMode", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setSpatializationBehavior", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStreamBuilder_setUsage", GetTrampolineFunc<auto(void*, int32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_close", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getAllowedCapturePolicy", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getBufferCapacityInFrames", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getBufferSizeInFrames", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getChannelCount", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getChannelMask", GetTrampolineFunc<auto(void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getContentType", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getDeviceId", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getDirection", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getFormat", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getFramesPerBurst", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getFramesPerDataCallback", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getFramesRead", GetTrampolineFunc<auto(void*) -> int64_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getFramesWritten", GetTrampolineFunc<auto(void*) -> int64_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getHardwareChannelCount", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getHardwareFormat", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getHardwareSampleRate", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getInputPreset", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getPerformanceMode", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getSampleRate", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getSamplesPerFrame", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getSessionId", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getSharingMode", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getSpatializationBehavior", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getState", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getTimestamp", GetTrampolineFunc<auto(void*, int32_t, void*, void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getUsage", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_getXRunCount", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_isContentSpatialized", GetTrampolineFunc<auto(void*) -> uint8_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_isMMapUsed", GetTrampolineFunc<auto(void*) -> uint8_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_isPrivacySensitive", GetTrampolineFunc<auto(void*) -> uint8_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_read", GetTrampolineFunc<auto(void*, void*, int32_t, int64_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_release", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_requestFlush", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_requestPause", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_requestStart", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_requestStop", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_setBufferSizeInFrames", GetTrampolineFunc<auto(void*, int32_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_waitForStateChange", GetTrampolineFunc<auto(void*, int32_t, void*, int64_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudioStream_write", GetTrampolineFunc<auto(void*, void*, int32_t, int64_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudio_convertResultToText", GetTrampolineFunc<auto(int32_t) -> void*>(), reinterpret_cast<void*>(NULL)},
{"AAudio_convertStreamStateToText", GetTrampolineFunc<auto(int32_t) -> void*>(), reinterpret_cast<void*>(NULL)},
{"AAudio_createStreamBuilder", GetTrampolineFunc<auto(void*) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudio_getMMapPolicy", GetTrampolineFunc<auto(void) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"AAudio_setMMapPolicy", GetTrampolineFunc<auto(int32_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
};  // kKnownTrampolines
const KnownVariable kKnownVariables[] = {
};  // kKnownVariables
// clang-format on
