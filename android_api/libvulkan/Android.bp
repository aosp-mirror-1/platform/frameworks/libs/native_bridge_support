//
// Copyright (C) 2018 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

cc_library {
    defaults: ["native_bridge_stub_library_defaults"],
    name: "libnative_bridge_guest_libvulkan",
    overrides: ["libvulkan"],
    stem: "libvulkan",
    arch: {
        arm: {
            srcs: ["stubs_arm.cc"],
        },
        arm64: {
            srcs: ["stubs_arm64.cc"],
        },
        riscv64: {
            srcs: ["stubs_riscv64.cc"],
        },
    },
    shared_libs: [
        // libbinder.so — greylisted, not included.
        "libcutils",
        "liblog",
        "libnative_bridge_guest_libnativewindow",
        // libui.so — greylisted, not included.
        "libutils",
    ],
}

bootstrap_go_package {
    name: "cpp_types",
    pkgPath: "berberis/cpp_types",
    srcs: [
        "proxy/cpp_types.go",
    ],
    testSrcs: [
        "proxy/cpp_types_test.go",
    ],
}

bootstrap_go_package {
    name: "vulkan_types",
    pkgPath: "berberis/vulkan_types",
    srcs: [
        "proxy/vulkan_types.go",
    ],
    deps: ["cpp_types"],
}

bootstrap_go_package {
    name: "vulkan_xml",
    pkgPath: "berberis/vulkan_xml",
    srcs: [
        "proxy/vulkan_xml.go",
    ],
    deps: [
        "cpp_types",
        "vulkan_types",
    ],
    testSrcs: [
        "proxy/vulkan_xml_test.go",
    ],
}

blueprint_go_binary {
    name: "gen_vulkan",
    srcs: ["proxy/gen_vulkan.go"],
    testSrcs: ["proxy/gen_vulkan_test.go"],
    deps: ["vulkan_xml"],
}

filegroup {
    name: "native_bridge_proxy_libvulkan_files",
    srcs: [
        "proxy/vulkan_trampolines.cc",
    ],
}

filegroup {
    name: "native_bridge_proxy_libvulkan_checker",
    srcs: [
        "proxy/emulated_api_checker.cc",
    ],
}
