// clang-format off
const KnownTrampoline kKnownTrampolines[] = {
{"android_convertGralloc0To1Usage", GetTrampolineFunc<auto(int32_t, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"android_convertGralloc1To0Usage", GetTrampolineFunc<auto(uint64_t, uint64_t) -> int32_t>(), reinterpret_cast<void*>(NULL)},
{"vkAcquireNextImage2KHR", DoCustomTrampolineWithThunk_vkAcquireNextImage2KHR, reinterpret_cast<void*>(vkAcquireNextImage2KHR)},
{"vkAcquireNextImageKHR", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, uint64_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkAllocateCommandBuffers", DoCustomTrampolineWithThunk_vkAllocateCommandBuffers, reinterpret_cast<void*>(vkAllocateCommandBuffers)},
{"vkAllocateDescriptorSets", DoCustomTrampolineWithThunk_vkAllocateDescriptorSets, reinterpret_cast<void*>(vkAllocateDescriptorSets)},
{"vkAllocateMemory", DoCustomTrampolineWithThunk_vkAllocateMemory, reinterpret_cast<void*>(vkAllocateMemory)},
{"vkBeginCommandBuffer", DoCustomTrampolineWithThunk_vkBeginCommandBuffer, reinterpret_cast<void*>(vkBeginCommandBuffer)},
{"vkBindBufferMemory", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkBindBufferMemory2", DoCustomTrampolineWithThunk_vkBindBufferMemory2, reinterpret_cast<void*>(vkBindBufferMemory2)},
{"vkBindImageMemory", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkBindImageMemory2", DoCustomTrampolineWithThunk_vkBindImageMemory2, reinterpret_cast<void*>(vkBindImageMemory2)},
{"vkCmdBeginQuery", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBeginRenderPass", DoCustomTrampolineWithThunk_vkCmdBeginRenderPass, reinterpret_cast<void*>(vkCmdBeginRenderPass)},
{"vkCmdBeginRenderPass2", DoCustomTrampolineWithThunk_vkCmdBeginRenderPass2, reinterpret_cast<void*>(vkCmdBeginRenderPass2)},
{"vkCmdBeginRendering", DoCustomTrampolineWithThunk_vkCmdBeginRendering, reinterpret_cast<void*>(vkCmdBeginRendering)},
{"vkCmdBindDescriptorSets", GetTrampolineFunc<auto(void*, uint32_t, uint64_t, uint32_t, uint32_t, void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBindIndexBuffer", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBindPipeline", GetTrampolineFunc<auto(void*, uint32_t, uint64_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBindVertexBuffers", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBindVertexBuffers2", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, void*, void*, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBlitImage", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint64_t, uint32_t, uint32_t, void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdBlitImage2", DoCustomTrampolineWithThunk_vkCmdBlitImage2, reinterpret_cast<void*>(vkCmdBlitImage2)},
{"vkCmdClearAttachments", GetTrampolineFunc<auto(void*, uint32_t, void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdClearColorImage", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdClearDepthStencilImage", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdCopyBuffer", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdCopyBuffer2", DoCustomTrampolineWithThunk_vkCmdCopyBuffer2, reinterpret_cast<void*>(vkCmdCopyBuffer2)},
{"vkCmdCopyBufferToImage", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdCopyBufferToImage2", DoCustomTrampolineWithThunk_vkCmdCopyBufferToImage2, reinterpret_cast<void*>(vkCmdCopyBufferToImage2)},
{"vkCmdCopyImage", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint64_t, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdCopyImage2", DoCustomTrampolineWithThunk_vkCmdCopyImage2, reinterpret_cast<void*>(vkCmdCopyImage2)},
{"vkCmdCopyImageToBuffer", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint64_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdCopyImageToBuffer2", DoCustomTrampolineWithThunk_vkCmdCopyImageToBuffer2, reinterpret_cast<void*>(vkCmdCopyImageToBuffer2)},
{"vkCmdCopyQueryPoolResults", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t, uint64_t, uint64_t, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDispatch", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDispatchBase", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDispatchIndirect", GetTrampolineFunc<auto(void*, uint64_t, uint64_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDraw", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDrawIndexed", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, int32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDrawIndexedIndirect", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDrawIndexedIndirectCount", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDrawIndirect", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdDrawIndirectCount", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdEndQuery", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdEndRenderPass", GetTrampolineFunc<auto(void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdEndRenderPass2", DoCustomTrampolineWithThunk_vkCmdEndRenderPass2, reinterpret_cast<void*>(vkCmdEndRenderPass2)},
{"vkCmdEndRendering", GetTrampolineFunc<auto(void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdExecuteCommands", GetTrampolineFunc<auto(void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdFillBuffer", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdNextSubpass", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdNextSubpass2", DoCustomTrampolineWithThunk_vkCmdNextSubpass2, reinterpret_cast<void*>(vkCmdNextSubpass2)},
{"vkCmdPipelineBarrier", DoCustomTrampolineWithThunk_vkCmdPipelineBarrier, reinterpret_cast<void*>(vkCmdPipelineBarrier)},
{"vkCmdPipelineBarrier2", DoCustomTrampolineWithThunk_vkCmdPipelineBarrier2, reinterpret_cast<void*>(vkCmdPipelineBarrier2)},
{"vkCmdPushConstants", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdResetEvent", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdResetEvent2", GetTrampolineFunc<auto(void*, uint64_t, uint64_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdResetQueryPool", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdResolveImage", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint64_t, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdResolveImage2", DoCustomTrampolineWithThunk_vkCmdResolveImage2, reinterpret_cast<void*>(vkCmdResolveImage2)},
{"vkCmdSetBlendConstants", GetTrampolineFunc<auto(void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetCullMode", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthBias", GetTrampolineFunc<auto(void*, float, float, float) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthBiasEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthBounds", GetTrampolineFunc<auto(void*, float, float) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthBoundsTestEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthCompareOp", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthTestEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDepthWriteEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetDeviceMask", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetEvent", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetEvent2", DoCustomTrampolineWithThunk_vkCmdSetEvent2, reinterpret_cast<void*>(vkCmdSetEvent2)},
{"vkCmdSetFrontFace", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetLineWidth", GetTrampolineFunc<auto(void*, float) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetPrimitiveRestartEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetPrimitiveTopology", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetRasterizerDiscardEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetScissor", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetScissorWithCount", GetTrampolineFunc<auto(void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetStencilCompareMask", GetTrampolineFunc<auto(void*, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetStencilOp", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetStencilReference", GetTrampolineFunc<auto(void*, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetStencilTestEnable", GetTrampolineFunc<auto(void*, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetStencilWriteMask", GetTrampolineFunc<auto(void*, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetViewport", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdSetViewportWithCount", GetTrampolineFunc<auto(void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdUpdateBuffer", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdWaitEvents", DoCustomTrampolineWithThunk_vkCmdWaitEvents, reinterpret_cast<void*>(vkCmdWaitEvents)},
{"vkCmdWaitEvents2", DoCustomTrampolineWithThunk_vkCmdWaitEvents2, reinterpret_cast<void*>(vkCmdWaitEvents2)},
{"vkCmdWriteTimestamp", GetTrampolineFunc<auto(void*, uint32_t, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCmdWriteTimestamp2", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkCreateAndroidSurfaceKHR", DoCustomTrampolineWithThunk_vkCreateAndroidSurfaceKHR, reinterpret_cast<void*>(vkCreateAndroidSurfaceKHR)},
{"vkCreateBuffer", DoCustomTrampolineWithThunk_vkCreateBuffer, reinterpret_cast<void*>(vkCreateBuffer)},
{"vkCreateBufferView", DoCustomTrampolineWithThunk_vkCreateBufferView, reinterpret_cast<void*>(vkCreateBufferView)},
{"vkCreateCommandPool", DoCustomTrampolineWithThunk_vkCreateCommandPool, reinterpret_cast<void*>(vkCreateCommandPool)},
{"vkCreateComputePipelines", DoCustomTrampolineWithThunk_vkCreateComputePipelines, reinterpret_cast<void*>(vkCreateComputePipelines)},
{"vkCreateDescriptorPool", DoCustomTrampolineWithThunk_vkCreateDescriptorPool, reinterpret_cast<void*>(vkCreateDescriptorPool)},
{"vkCreateDescriptorSetLayout", DoCustomTrampolineWithThunk_vkCreateDescriptorSetLayout, reinterpret_cast<void*>(vkCreateDescriptorSetLayout)},
{"vkCreateDescriptorUpdateTemplate", DoCustomTrampolineWithThunk_vkCreateDescriptorUpdateTemplate, reinterpret_cast<void*>(vkCreateDescriptorUpdateTemplate)},
{"vkCreateDevice", DoCustomTrampolineWithThunk_vkCreateDevice, reinterpret_cast<void*>(vkCreateDevice)},
{"vkCreateEvent", DoCustomTrampolineWithThunk_vkCreateEvent, reinterpret_cast<void*>(vkCreateEvent)},
{"vkCreateFence", DoCustomTrampolineWithThunk_vkCreateFence, reinterpret_cast<void*>(vkCreateFence)},
{"vkCreateFramebuffer", DoCustomTrampolineWithThunk_vkCreateFramebuffer, reinterpret_cast<void*>(vkCreateFramebuffer)},
{"vkCreateGraphicsPipelines", DoCustomTrampolineWithThunk_vkCreateGraphicsPipelines, reinterpret_cast<void*>(vkCreateGraphicsPipelines)},
{"vkCreateImage", DoCustomTrampolineWithThunk_vkCreateImage, reinterpret_cast<void*>(vkCreateImage)},
{"vkCreateImageView", DoCustomTrampolineWithThunk_vkCreateImageView, reinterpret_cast<void*>(vkCreateImageView)},
{"vkCreateInstance", DoCustomTrampolineWithThunk_vkCreateInstance, reinterpret_cast<void*>(vkCreateInstance)},
{"vkCreatePipelineCache", DoCustomTrampolineWithThunk_vkCreatePipelineCache, reinterpret_cast<void*>(vkCreatePipelineCache)},
{"vkCreatePipelineLayout", DoCustomTrampolineWithThunk_vkCreatePipelineLayout, reinterpret_cast<void*>(vkCreatePipelineLayout)},
{"vkCreatePrivateDataSlot", DoCustomTrampolineWithThunk_vkCreatePrivateDataSlot, reinterpret_cast<void*>(vkCreatePrivateDataSlot)},
{"vkCreateQueryPool", DoCustomTrampolineWithThunk_vkCreateQueryPool, reinterpret_cast<void*>(vkCreateQueryPool)},
{"vkCreateRenderPass", DoCustomTrampolineWithThunk_vkCreateRenderPass, reinterpret_cast<void*>(vkCreateRenderPass)},
{"vkCreateRenderPass2", DoCustomTrampolineWithThunk_vkCreateRenderPass2, reinterpret_cast<void*>(vkCreateRenderPass2)},
{"vkCreateSampler", DoCustomTrampolineWithThunk_vkCreateSampler, reinterpret_cast<void*>(vkCreateSampler)},
{"vkCreateSamplerYcbcrConversion", DoCustomTrampolineWithThunk_vkCreateSamplerYcbcrConversion, reinterpret_cast<void*>(vkCreateSamplerYcbcrConversion)},
{"vkCreateSemaphore", DoCustomTrampolineWithThunk_vkCreateSemaphore, reinterpret_cast<void*>(vkCreateSemaphore)},
{"vkCreateShaderModule", DoCustomTrampolineWithThunk_vkCreateShaderModule, reinterpret_cast<void*>(vkCreateShaderModule)},
{"vkCreateSwapchainKHR", DoCustomTrampolineWithThunk_vkCreateSwapchainKHR, reinterpret_cast<void*>(vkCreateSwapchainKHR)},
{"vkDestroyBuffer", DoCustomTrampolineWithThunk_vkDestroyBuffer, reinterpret_cast<void*>(vkDestroyBuffer)},
{"vkDestroyBufferView", DoCustomTrampolineWithThunk_vkDestroyBufferView, reinterpret_cast<void*>(vkDestroyBufferView)},
{"vkDestroyCommandPool", DoCustomTrampolineWithThunk_vkDestroyCommandPool, reinterpret_cast<void*>(vkDestroyCommandPool)},
{"vkDestroyDescriptorPool", DoCustomTrampolineWithThunk_vkDestroyDescriptorPool, reinterpret_cast<void*>(vkDestroyDescriptorPool)},
{"vkDestroyDescriptorSetLayout", DoCustomTrampolineWithThunk_vkDestroyDescriptorSetLayout, reinterpret_cast<void*>(vkDestroyDescriptorSetLayout)},
{"vkDestroyDescriptorUpdateTemplate", DoCustomTrampolineWithThunk_vkDestroyDescriptorUpdateTemplate, reinterpret_cast<void*>(vkDestroyDescriptorUpdateTemplate)},
{"vkDestroyDevice", DoCustomTrampolineWithThunk_vkDestroyDevice, reinterpret_cast<void*>(vkDestroyDevice)},
{"vkDestroyEvent", DoCustomTrampolineWithThunk_vkDestroyEvent, reinterpret_cast<void*>(vkDestroyEvent)},
{"vkDestroyFence", DoCustomTrampolineWithThunk_vkDestroyFence, reinterpret_cast<void*>(vkDestroyFence)},
{"vkDestroyFramebuffer", DoCustomTrampolineWithThunk_vkDestroyFramebuffer, reinterpret_cast<void*>(vkDestroyFramebuffer)},
{"vkDestroyImage", DoCustomTrampolineWithThunk_vkDestroyImage, reinterpret_cast<void*>(vkDestroyImage)},
{"vkDestroyImageView", DoCustomTrampolineWithThunk_vkDestroyImageView, reinterpret_cast<void*>(vkDestroyImageView)},
{"vkDestroyInstance", DoCustomTrampolineWithThunk_vkDestroyInstance, reinterpret_cast<void*>(vkDestroyInstance)},
{"vkDestroyPipeline", DoCustomTrampolineWithThunk_vkDestroyPipeline, reinterpret_cast<void*>(vkDestroyPipeline)},
{"vkDestroyPipelineCache", DoCustomTrampolineWithThunk_vkDestroyPipelineCache, reinterpret_cast<void*>(vkDestroyPipelineCache)},
{"vkDestroyPipelineLayout", DoCustomTrampolineWithThunk_vkDestroyPipelineLayout, reinterpret_cast<void*>(vkDestroyPipelineLayout)},
{"vkDestroyPrivateDataSlot", DoCustomTrampolineWithThunk_vkDestroyPrivateDataSlot, reinterpret_cast<void*>(vkDestroyPrivateDataSlot)},
{"vkDestroyQueryPool", DoCustomTrampolineWithThunk_vkDestroyQueryPool, reinterpret_cast<void*>(vkDestroyQueryPool)},
{"vkDestroyRenderPass", DoCustomTrampolineWithThunk_vkDestroyRenderPass, reinterpret_cast<void*>(vkDestroyRenderPass)},
{"vkDestroySampler", DoCustomTrampolineWithThunk_vkDestroySampler, reinterpret_cast<void*>(vkDestroySampler)},
{"vkDestroySamplerYcbcrConversion", DoCustomTrampolineWithThunk_vkDestroySamplerYcbcrConversion, reinterpret_cast<void*>(vkDestroySamplerYcbcrConversion)},
{"vkDestroySemaphore", DoCustomTrampolineWithThunk_vkDestroySemaphore, reinterpret_cast<void*>(vkDestroySemaphore)},
{"vkDestroyShaderModule", DoCustomTrampolineWithThunk_vkDestroyShaderModule, reinterpret_cast<void*>(vkDestroyShaderModule)},
{"vkDestroySurfaceKHR", DoCustomTrampolineWithThunk_vkDestroySurfaceKHR, reinterpret_cast<void*>(vkDestroySurfaceKHR)},
{"vkDestroySwapchainKHR", DoCustomTrampolineWithThunk_vkDestroySwapchainKHR, reinterpret_cast<void*>(vkDestroySwapchainKHR)},
{"vkDeviceWaitIdle", GetTrampolineFunc<auto(void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEndCommandBuffer", GetTrampolineFunc<auto(void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumerateDeviceExtensionProperties", GetTrampolineFunc<auto(void*, void*, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumerateDeviceLayerProperties", GetTrampolineFunc<auto(void*, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumerateInstanceExtensionProperties", GetTrampolineFunc<auto(void*, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumerateInstanceLayerProperties", GetTrampolineFunc<auto(void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumerateInstanceVersion", GetTrampolineFunc<auto(void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkEnumeratePhysicalDeviceGroups", DoCustomTrampolineWithThunk_vkEnumeratePhysicalDeviceGroups, reinterpret_cast<void*>(vkEnumeratePhysicalDeviceGroups)},
{"vkEnumeratePhysicalDevices", GetTrampolineFunc<auto(void*, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkFlushMappedMemoryRanges", DoCustomTrampolineWithThunk_vkFlushMappedMemoryRanges, reinterpret_cast<void*>(vkFlushMappedMemoryRanges)},
{"vkFreeCommandBuffers", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkFreeDescriptorSets", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkFreeMemory", DoCustomTrampolineWithThunk_vkFreeMemory, reinterpret_cast<void*>(vkFreeMemory)},
{"vkGetAndroidHardwareBufferPropertiesANDROID", DoCustomTrampolineWithThunk_vkGetAndroidHardwareBufferPropertiesANDROID, reinterpret_cast<void*>(vkGetAndroidHardwareBufferPropertiesANDROID)},
{"vkGetBufferDeviceAddress", DoCustomTrampolineWithThunk_vkGetBufferDeviceAddress, reinterpret_cast<void*>(vkGetBufferDeviceAddress)},
{"vkGetBufferMemoryRequirements", DoCustomTrampolineWithThunk_vkGetBufferMemoryRequirements, reinterpret_cast<void*>(vkGetBufferMemoryRequirements)},
{"vkGetBufferMemoryRequirements2", DoCustomTrampolineWithThunk_vkGetBufferMemoryRequirements2, reinterpret_cast<void*>(vkGetBufferMemoryRequirements2)},
{"vkGetBufferOpaqueCaptureAddress", DoCustomTrampolineWithThunk_vkGetBufferOpaqueCaptureAddress, reinterpret_cast<void*>(vkGetBufferOpaqueCaptureAddress)},
{"vkGetDescriptorSetLayoutSupport", DoCustomTrampolineWithThunk_vkGetDescriptorSetLayoutSupport, reinterpret_cast<void*>(vkGetDescriptorSetLayoutSupport)},
{"vkGetDeviceBufferMemoryRequirements", DoCustomTrampolineWithThunk_vkGetDeviceBufferMemoryRequirements, reinterpret_cast<void*>(vkGetDeviceBufferMemoryRequirements)},
{"vkGetDeviceGroupPeerMemoryFeatures", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetDeviceGroupPresentCapabilitiesKHR", DoCustomTrampolineWithThunk_vkGetDeviceGroupPresentCapabilitiesKHR, reinterpret_cast<void*>(vkGetDeviceGroupPresentCapabilitiesKHR)},
{"vkGetDeviceGroupSurfacePresentModesKHR", GetTrampolineFunc<auto(void*, uint64_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetDeviceImageMemoryRequirements", DoCustomTrampolineWithThunk_vkGetDeviceImageMemoryRequirements, reinterpret_cast<void*>(vkGetDeviceImageMemoryRequirements)},
{"vkGetDeviceImageSparseMemoryRequirements", DoCustomTrampolineWithThunk_vkGetDeviceImageSparseMemoryRequirements, reinterpret_cast<void*>(vkGetDeviceImageSparseMemoryRequirements)},
{"vkGetDeviceMemoryCommitment", GetTrampolineFunc<auto(void*, uint64_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetDeviceMemoryOpaqueCaptureAddress", DoCustomTrampolineWithThunk_vkGetDeviceMemoryOpaqueCaptureAddress, reinterpret_cast<void*>(vkGetDeviceMemoryOpaqueCaptureAddress)},
{"vkGetDeviceProcAddr", DoCustomTrampolineWithThunk_vkGetDeviceProcAddr, reinterpret_cast<void*>(vkGetDeviceProcAddr)},
{"vkGetDeviceQueue", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetDeviceQueue2", DoCustomTrampolineWithThunk_vkGetDeviceQueue2, reinterpret_cast<void*>(vkGetDeviceQueue2)},
{"vkGetEventStatus", GetTrampolineFunc<auto(void*, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetFenceStatus", GetTrampolineFunc<auto(void*, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetImageMemoryRequirements", DoCustomTrampolineWithThunk_vkGetImageMemoryRequirements, reinterpret_cast<void*>(vkGetImageMemoryRequirements)},
{"vkGetImageMemoryRequirements2", DoCustomTrampolineWithThunk_vkGetImageMemoryRequirements2, reinterpret_cast<void*>(vkGetImageMemoryRequirements2)},
{"vkGetImageSparseMemoryRequirements", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetImageSparseMemoryRequirements2", DoCustomTrampolineWithThunk_vkGetImageSparseMemoryRequirements2, reinterpret_cast<void*>(vkGetImageSparseMemoryRequirements2)},
{"vkGetImageSubresourceLayout", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetInstanceProcAddr", DoCustomTrampolineWithThunk_vkGetInstanceProcAddr, reinterpret_cast<void*>(vkGetInstanceProcAddr)},
{"vkGetMemoryAndroidHardwareBufferANDROID", DoCustomTrampolineWithThunk_vkGetMemoryAndroidHardwareBufferANDROID, reinterpret_cast<void*>(vkGetMemoryAndroidHardwareBufferANDROID)},
{"vkGetPhysicalDeviceExternalBufferProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceExternalBufferProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceExternalBufferProperties)},
{"vkGetPhysicalDeviceExternalFenceProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceExternalFenceProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceExternalFenceProperties)},
{"vkGetPhysicalDeviceExternalSemaphoreProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceExternalSemaphoreProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceExternalSemaphoreProperties)},
{"vkGetPhysicalDeviceFeatures", GetTrampolineFunc<auto(void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceFeatures2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceFeatures2, reinterpret_cast<void*>(vkGetPhysicalDeviceFeatures2)},
{"vkGetPhysicalDeviceFormatProperties", GetTrampolineFunc<auto(void*, uint32_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceFormatProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceFormatProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceFormatProperties2)},
{"vkGetPhysicalDeviceImageFormatProperties", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceImageFormatProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceImageFormatProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceImageFormatProperties2)},
{"vkGetPhysicalDeviceMemoryProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceMemoryProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceMemoryProperties)},
{"vkGetPhysicalDeviceMemoryProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceMemoryProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceMemoryProperties2)},
{"vkGetPhysicalDevicePresentRectanglesKHR", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceProperties)},
{"vkGetPhysicalDeviceProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceProperties2)},
{"vkGetPhysicalDeviceQueueFamilyProperties", GetTrampolineFunc<auto(void*, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceQueueFamilyProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceQueueFamilyProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceQueueFamilyProperties2)},
{"vkGetPhysicalDeviceSparseImageFormatProperties", GetTrampolineFunc<auto(void*, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, void*, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceSparseImageFormatProperties2", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceSparseImageFormatProperties2, reinterpret_cast<void*>(vkGetPhysicalDeviceSparseImageFormatProperties2)},
{"vkGetPhysicalDeviceSurfaceCapabilitiesKHR", GetTrampolineFunc<auto(void*, uint64_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceSurfaceFormatsKHR", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceSurfacePresentModesKHR", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceSurfaceSupportKHR", GetTrampolineFunc<auto(void*, uint32_t, uint64_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPhysicalDeviceToolProperties", DoCustomTrampolineWithThunk_vkGetPhysicalDeviceToolProperties, reinterpret_cast<void*>(vkGetPhysicalDeviceToolProperties)},
{"vkGetPipelineCacheData", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetPrivateData", GetTrampolineFunc<auto(void*, uint32_t, uint64_t, uint64_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetQueryPoolResults", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t, uint32_t, void*, uint64_t, uint32_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetRenderAreaGranularity", GetTrampolineFunc<auto(void*, uint64_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkGetSemaphoreCounterValue", GetTrampolineFunc<auto(void*, uint64_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkGetSwapchainImagesKHR", GetTrampolineFunc<auto(void*, uint64_t, void*, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkInvalidateMappedMemoryRanges", DoCustomTrampolineWithThunk_vkInvalidateMappedMemoryRanges, reinterpret_cast<void*>(vkInvalidateMappedMemoryRanges)},
{"vkMapMemory", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, uint64_t, uint32_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkMergePipelineCaches", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkQueueBindSparse", DoCustomTrampolineWithThunk_vkQueueBindSparse, reinterpret_cast<void*>(vkQueueBindSparse)},
{"vkQueuePresentKHR", DoCustomTrampolineWithThunk_vkQueuePresentKHR, reinterpret_cast<void*>(vkQueuePresentKHR)},
{"vkQueueSubmit", DoCustomTrampolineWithThunk_vkQueueSubmit, reinterpret_cast<void*>(vkQueueSubmit)},
{"vkQueueSubmit2", DoCustomTrampolineWithThunk_vkQueueSubmit2, reinterpret_cast<void*>(vkQueueSubmit2)},
{"vkQueueWaitIdle", GetTrampolineFunc<auto(void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetCommandBuffer", GetTrampolineFunc<auto(void*, uint32_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetCommandPool", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetDescriptorPool", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetEvent", GetTrampolineFunc<auto(void*, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetFences", GetTrampolineFunc<auto(void*, uint32_t, void*) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkResetQueryPool", GetTrampolineFunc<auto(void*, uint64_t, uint32_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkSetEvent", GetTrampolineFunc<auto(void*, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkSetPrivateData", GetTrampolineFunc<auto(void*, uint32_t, uint64_t, uint64_t, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkSignalSemaphore", DoCustomTrampolineWithThunk_vkSignalSemaphore, reinterpret_cast<void*>(vkSignalSemaphore)},
{"vkTrimCommandPool", GetTrampolineFunc<auto(void*, uint64_t, uint32_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkUnmapMemory", GetTrampolineFunc<auto(void*, uint64_t) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkUpdateDescriptorSetWithTemplate", GetTrampolineFunc<auto(void*, uint64_t, uint64_t, void*) -> void>(), reinterpret_cast<void*>(NULL)},
{"vkUpdateDescriptorSets", DoCustomTrampolineWithThunk_vkUpdateDescriptorSets, reinterpret_cast<void*>(vkUpdateDescriptorSets)},
{"vkWaitForFences", GetTrampolineFunc<auto(void*, uint32_t, void*, uint32_t, uint64_t) -> uint32_t>(), reinterpret_cast<void*>(NULL)},
{"vkWaitSemaphores", DoCustomTrampolineWithThunk_vkWaitSemaphores, reinterpret_cast<void*>(vkWaitSemaphores)},
};  // kKnownTrampolines
const KnownVariable kKnownVariables[] = {
};  // kKnownVariables
// clang-format on
