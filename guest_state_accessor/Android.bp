//
// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

cc_library_headers {
    name: "libnative_bridge_support_accessor_headers",
    defaults: ["native_bridge_support_defaults"],
    host_supported: true,
    export_include_dirs: ["include"],
    apex_available: [
        "com.android.runtime",
        "//apex_available:platform",
    ],
}

cc_library_static {
    name: "libnative_bridge_guest_state_accessor",
    defaults: ["native_bridge_support_defaults"],
    host_supported: true,
    header_libs: [
        "libnative_bridge_support_accessor_headers",
    ],
    srcs: ["accessor_proxy.cc"],
    shared_libs: [
        "libbase",
        "liblog",
    ],
    apex_available: [
        "com.android.runtime",
        "//apex_available:platform",
    ],
}

cc_test_host {
    name: "libnative_bridge_guest_state_accessor_tests",
    defaults: ["native_bridge_support_defaults"],
    header_libs: ["libnative_bridge_support_accessor_headers"],
    srcs: ["accessor_tests.cc"],
}
